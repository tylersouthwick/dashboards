package services

import shared.models.SharedDefault.BaseUser
import shared.models.Task
import shared.services.Api

import scala.concurrent.Future

class ApiService(user: Option[BaseUser]) extends Api {

  def ping(): Future[String] = "pong"

  override def tasks(): Future[Seq[Task]] = Seq(
    Task("test1")
  )
}
