package shared.services

import scala.concurrent.Future
import shared.models.Task

trait Api {
  def ping(): Future[String]

  def tasks() : Future[Seq[Task]]
}
