package components

import japgolly.scalajs.react._
import services.AjaxClient
import shared.services.Api
import shared.models.Task
import utils.ReactTags

object TaskListComponent extends ReactTags {

  case class Props()

  case class State(tasks : Option[Seq[Task]])

  class Backend($: BackendScope[Props, State]) {

    def start() = Callback {
      loadTasks()
    }

    def render(props: Props, state: State) = state.tasks match {
      case None =>
        <.div(
          <.h2("No Tasks found")
        )
      case Some(tasks) =>
        <.div(
          <.h2(s"Found ${tasks.size} tasks"),
          <.ul(
            tasks.map(task => <.li(task.description))
          )
        )
    }

    def loadTasks() = {
      import autowire._
      AjaxClient[Api].tasks().call().map(tasks => {
        $.modState(state => state.copy(tasks = Some(tasks))).runNow()
      })
    }

  }

  private val component = ReactComponentB[Props]("TaskListComponent")
    .initialState(State(tasks = None))
    .renderBackend[Backend]
    .componentDidMount(_.backend.start())
    .build

  def apply() = component.apply(Props())
}
