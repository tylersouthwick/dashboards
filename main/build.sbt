import sbt.Project.projectToRef

name in ThisBuild := """Northfuse Dashboard"""

version in ThisBuild := "1.0-SNAPSHOT"

scalaVersion in ThisBuild := "2.11.8"

resolvers in ThisBuild ++= Seq(
  "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases",
  Resolver.sonatypeRepo("snapshots")
)

lazy val web = (project in file("web"))
  .settings(
    persistLauncher := true,
    persistLauncher in Test := false,
    libraryDependencies ++= Seq(
      "com.github.japgolly.scalajs-react" %%% "core" % "0.11.1",
      "com.github.japgolly.scalajs-react" %%% "extra" % "0.11.1",
      "org.scala-js" %%% "scalajs-dom" % "0.9.1",
      "com.github.japgolly.fork.scalaz" %%% "scalaz-core" % "7.2.0"
    ),
    jsDependencies ++= Seq(
      "org.webjars.bower" % "react" % "0.14.7" / "react-with-addons.js" minified "react-with-addons.min.js" commonJSName "React",
      "org.webjars.bower" % "react" % "0.14.7" / "react-dom.js" minified "react-dom.min.js" dependsOn "react-with-addons.js" commonJSName "ReactDOM"
    )
  )
  .dependsOn(sharedJS)
  .enablePlugins(ScalaJSPlugin, ScalaJSPlay)

lazy val jsProjects = Seq(web)

lazy val server = (project in file("server"))
  .settings(
    libraryDependencies ++= Seq(
      jdbc,
      cache,
      ws,
      "com.vmunier" %% "play-scalajs-scripts" % "0.3.0",
      "jp.t2v" %% "play2-auth" % "0.14.1",
      "org.flywaydb" %% "flyway-play" % "2.2.1"
    ),
    scalaJSProjects := jsProjects,
    pipelineStages := Seq(scalaJSProd),
    routesGenerator := InjectedRoutesGenerator
  )
  .dependsOn(sharedJVM)
  .aggregate(jsProjects.map(projectToRef): _*)
  .enablePlugins(PlayScala, DockerPlugin, JavaServerAppPackaging)

lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared"))
  .settings(libraryDependencies ++= Seq(
    "com.lihaoyi" %%% "autowire" % "0.2.5",
    "com.lihaoyi" %%% "upickle" % "0.4.1",
    "com.lihaoyi" %%% "scalarx" % "0.3.1",
    "me.chrons" %%% "diode-data" % "1.0.0",
    "org.scalaz" %%% "scalaz-core" % "7.2.2"
  ))
  .jsConfigure(_ enablePlugins ScalaJSPlay)
  .jsSettings(
  )

lazy val sharedJVM = shared.jvm

lazy val sharedJS = shared.js

onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value
